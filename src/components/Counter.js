import React, { Component } from "react";
import { connect } from "react-redux";
import { Badge, Button } from "react-bootstrap";

class Counter extends Component {
  state = {};
  handleIncrement = () => {
    this.props.dispatch({ type: "INCREMENT", data: "Incremented" });
  };

  handleDecrement = () => {
    this.props.dispatch({ type: "DECREMENT", data: "Decremented" });
  };
  render() {
    return (
      <>
        <Button onClick={this.handleDecrement} variant="danger" size="sm">
          Decrement
        </Button>

        <Badge style={{ margin: "1rem" }} variant="info">
          {this.props.count}
        </Badge>

        <Button onClick={this.handleIncrement} variant="success" size="sm">
          Increment
        </Button>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  count: state.count,
});
export default connect(mapStateToProps)(Counter);
