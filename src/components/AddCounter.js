import React, { Component } from "react";
import { Button } from "react-bootstrap";

class AddCounter extends Component {
  render() {
    return (
      <Button style={{ margin: "2rem" }} variant="info">
        Add Counter
      </Button>
    );
  }
}

export default AddCounter;
