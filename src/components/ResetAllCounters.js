import React, { Component } from "react";
import { Button } from "react-bootstrap";

class ResetAllCounters extends Component {
  render() {
    return (
      <Button style={{ margin: "2rem" }} variant="danger">
        Reset All Counters
      </Button>
    );
  }
}

export default ResetAllCounters;
