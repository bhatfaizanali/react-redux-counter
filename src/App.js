import React, { Component } from "react";
import { Provider } from "react-redux";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import Counter from "./components/Counter";
import { store } from "./Store";
import AddCounter from "./components/AddCounter";
import ResetAllCounters from "./components/ResetAllCounters";

class App extends Component {
  constructor() {
    super();
    store.subscribe(() => {
      console.log(store.getState());
    });
  }

  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <h1 style={{ borderBottom: "0.1rem solid black", margin: "2rem" }}>
            Simple Redux Counter
          </h1>
          <div style={{ borderRadius: "1rem", border: "0.1rem  solid black" }}>
            <ResetAllCounters />
            <AddCounter />
          </div>
          <div
            style={{
              margin: "1rem",
              border: "0.1rem solid black",
            }}
          >
            <Counter />
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;
